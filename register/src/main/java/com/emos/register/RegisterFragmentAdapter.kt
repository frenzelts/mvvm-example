package com.emos.register

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

class RegisterFragmentAdapter(fragmentActivity: FragmentActivity, private val tabsCount: Int) : FragmentStateAdapter(fragmentActivity) {
    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> CustomerFragment.newInstance()
            1 -> UserFragment.newInstance()
            2 -> ReviewFragment.newInstance()
            else -> CustomerFragment.newInstance()
        }
    }

    override fun getItemCount(): Int {
        return tabsCount
    }
}
