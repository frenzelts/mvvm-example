package com.emos.register

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import com.emos.register.databinding.CustomerFragmentBinding
import com.emos.register.databinding.UserFragmentBinding
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class UserFragment : Fragment() {
    val TAG = "UserFragment"

    companion object{
        fun newInstance(): Fragment {
            return UserFragment()
        }
    }

    private var _binding : UserFragmentBinding? = null
    private val binding
        get() = _binding!!

    private val viewmodel : RegisterViewModel by viewModels()

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        _binding = UserFragmentBinding.inflate(inflater, container, false)
        return _binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initListeners()
    }

    private fun initListeners(){
        binding.etEmail.addTextChangedListener {
            viewmodel.setEmail(it.toString())
        }
        binding.etUsername.addTextChangedListener {
            viewmodel.setUsername(it.toString())
        }
    }
}