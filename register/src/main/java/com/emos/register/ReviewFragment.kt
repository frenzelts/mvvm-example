package com.emos.register

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.emos.register.databinding.ReviewFragmentBinding
import com.emos.register.databinding.UserFragmentBinding
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class ReviewFragment : Fragment() {
    val TAG = "ReviewFragment"

    companion object{
        fun newInstance(): Fragment {
            return ReviewFragment()
        }
    }

    private var _binding : ReviewFragmentBinding? = null
    private val binding
        get() = _binding!!

    private val viewmodel : RegisterViewModel by activityViewModels()

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        _binding = ReviewFragmentBinding.inflate(inflater, container, false)
        return _binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        observeData()
    }

    private fun observeData(){
        lifecycleScope.launch {
            lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED){
                launch {
                    viewmodel.fullname.collect { binding.tvFullname.text = "Fullname: $it" }
                }
                launch {
                    viewmodel.address.collect { binding.tvAddress.text = "Address: $it" }
                }
                launch {
                    viewmodel.email.collect { binding.tvEmail.text = "Email: $it" }
                }
                launch {
                    viewmodel.username.collect { binding.tvUsername.text = "Username: $it" }
                }
            }
        }
    }
}