package com.emos.register

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class RegisterViewModel : ViewModel() {
    companion object{
        const val LAST_PAGE = 3
        const val ACTION_NEXT = "NEXT"
        const val ACTION_BACK = "BACK"
        const val ACTION_SUBMIT = "SUBMIT"
    }
    private var _currentPage = MutableStateFlow<Int>(1)
    val currentPage : StateFlow<Int> = _currentPage
    fun nextPage(){
        viewModelScope.launch {
            val currentPage = currentPage.value
            _currentPage.emit(currentPage+1)
        }
    }
    fun backPage(){
        viewModelScope.launch {
            val currentPage = currentPage.value
            _currentPage.emit(currentPage-1)
        }
    }

    private var _fullname = MutableStateFlow<String?>(null)
    val fullname : StateFlow<String?> = _fullname
    fun setFullname(value: String) = viewModelScope.launch {
        _fullname.emit(value)
    }

    private var _address = MutableStateFlow<String?>(null)
    val address : StateFlow<String?> = _address
    fun setAddress(value: String) = viewModelScope.launch {
        _address.emit(value)
    }

    private var _email = MutableStateFlow<String?>(null)
    val email : StateFlow<String?> = _email
    fun setEmail(value: String) = viewModelScope.launch {
        _email.emit(value)
    }

    private var _username = MutableStateFlow<String?>(null)
    val username : StateFlow<String?> = _username
    fun setUsername(value: String) = viewModelScope.launch {
        _username.emit(value)
    }
}