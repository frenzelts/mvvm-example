package com.emos.register

import android.R
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import com.emos.register.databinding.ActivityRegisterBinding
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class RegisterActivity : AppCompatActivity() {
    val TAG = "RegisterActivity"

    private lateinit var binding: ActivityRegisterBinding
    private val fragmentAdapter by lazy { RegisterFragmentAdapter(this, RegisterViewModel.LAST_PAGE) }

    private val viewmodel : RegisterViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar?.title = "Register Example"

        setupNavigation()
        initListeners()
        observePageChanges()
    }

    private fun setupNavigation(){
        binding.viewpager.adapter = fragmentAdapter
        binding.viewpager.isUserInputEnabled = false
    }

    private fun initListeners(){
        binding.btnNext.setOnClickListener {
            viewmodel.nextPage()
        }
        binding.btnBack.setOnClickListener {
            viewmodel.backPage()
        }
    }

    private fun observePageChanges(){
        lifecycleScope.launch {
            viewmodel.currentPage
                    .flowWithLifecycle(lifecycle)
                    .collect {
                        if(it>RegisterViewModel.LAST_PAGE || it<1){
                            finish()
                        } else {
                            binding.viewpager.currentItem = it-1
                        }

//                        Navigation.findNavController(binding.content).popBackStack()

                        binding.btnBack.apply {
                            visibility = if(it==1) View.INVISIBLE else View.VISIBLE
                        }
                        binding.btnNext.apply {
                            text = if(it==RegisterViewModel.LAST_PAGE) "Submit" else "Next"
                        }
                    }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.home) {
            finish()
        }
        return true
    }

}