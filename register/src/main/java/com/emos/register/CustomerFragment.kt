package com.emos.register

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.viewModels
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import com.emos.register.databinding.CustomerFragmentBinding
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class CustomerFragment : Fragment() {
    val TAG = "CustomerFragment"

    companion object{
        fun newInstance(): Fragment {
            return CustomerFragment()
        }
    }

    private var _binding : CustomerFragmentBinding? = null
    private val binding
        get() = _binding!!

    private val viewmodel : RegisterViewModel by activityViewModels()

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        _binding = CustomerFragmentBinding.inflate(inflater, container, false)
        return _binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initListeners()
    }

    private fun initListeners(){
        binding.etFullname.addTextChangedListener {
            viewmodel.setFullname(it.toString())
        }
        binding.etAddress.addTextChangedListener {
            viewmodel.setAddress(it.toString())
        }
    }
}