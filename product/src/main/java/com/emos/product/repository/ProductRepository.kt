package com.emos.product.repository

import android.app.Application
import com.google.gson.Gson
import com.emos.product.R
import com.emos.product.model.ProductModel
import com.emos.product.model.ProductsDto
import com.emos.product.util.ProductMapper
import com.emos.product.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.withContext
import java.io.InputStreamReader

class ProductRepository(
    val context: Application
) {
    val TAG = "ProductRepository"

    suspend fun fetchProducts() = withContext(Dispatchers.IO){
        Resource.Success(readJson())
    }

    private fun readJson() : List<ProductModel>{
        val stream = context.resources.openRawResource(R.raw.products)
        val reader = InputStreamReader(stream)
        val model = Gson().fromJson(reader, ProductsDto::class.java)
        reader.close()
        stream.close()
        return ProductMapper.mapToDomainList(model.data.products)
    }
}