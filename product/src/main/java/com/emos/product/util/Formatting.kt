package com.emos.product.util

import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

object Formatting {
    fun currencyFormat(value: Int) : String{
        val locale = Locale("in", "ID")
        val decimalFormat = NumberFormat.getInstance(locale) as DecimalFormat
        val symbol = Currency.getInstance(locale).getSymbol(locale)
        decimalFormat.isGroupingUsed = true
        decimalFormat.positivePrefix = "$symbol "
        decimalFormat.negativePrefix = "-$symbol "
        return decimalFormat.format(value)
    }
}