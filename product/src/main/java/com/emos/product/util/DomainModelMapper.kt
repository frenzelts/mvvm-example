package com.emos.product.util

interface DomainModelMapper<Dto, DomainModel> {
    fun mapToDomain(dto: Dto) : DomainModel
    fun mapToDomainList(list: List<Dto>?) : List<DomainModel>
}