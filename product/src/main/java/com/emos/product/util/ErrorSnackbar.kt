package com.emos.product.util

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.google.android.material.snackbar.Snackbar
import com.emos.product.R

object ErrorSnackbar {
    fun make(parent: ViewGroup, layoutInflater: LayoutInflater, message: String, duration: Int,
             buttonClickListener: () -> Unit) : Snackbar {
        val snackbar = Snackbar.make(parent, " ", duration)
        val customView = layoutInflater.inflate(R.layout.snackbar_error, parent, false)
        snackbar.view.setBackgroundColor(Color.TRANSPARENT)
        val snackBarView = snackbar.view as Snackbar.SnackbarLayout
        snackBarView.setPadding(0, 0, 0, 0)
        (customView.findViewById(R.id.message) as TextView).text = message
        (customView.findViewById(R.id.separator) as View).visibility = View.VISIBLE
        (customView.findViewById(R.id.btn_retry) as TextView).setOnClickListener(View.OnClickListener {
            buttonClickListener()
            snackbar.dismiss()
        })
        snackBarView.addView(customView, 0)
        return snackbar
    }


}