package com.emos.product.viewmodel

import androidx.lifecycle.*
import com.emos.product.model.ProductModel
import com.emos.product.repository.ProductRepository
import com.emos.product.util.Resource
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class ProductViewModel(
    private val repository: ProductRepository
) : ViewModel() {
    val TAG = "ProductViewModel"

    //All Products Flow (from original source, unfiltered)
    private val _products = MutableStateFlow<Resource<List<ProductModel>>>(Resource.Loading())

    private var _keyword = MutableStateFlow<String>("")
    val keyword : StateFlow<String> = _keyword
    fun searchKeyword(keyword: String){
        viewModelScope.launch {
            _keyword.emit(keyword)
        }
    }

    private var _filterTipeHazard = MutableStateFlow<String?>(null)
    val filterTipeHazard = combine(_filterTipeHazard, _products){
        filter, data ->
        data.let {
            it.data?.let { it.map { it.tipe }.distinct() }
                ?.map { Pair(it, it == filter ?: false) }.orEmpty()
        }
    }

    //Filtered Products LiveData to be consumed by view layer
    val products : StateFlow<Resource<List<ProductModel>>> = combine(_products, _filterTipeHazard, keyword){
        product, filterTipe, keyword ->
        if(product is Resource.Success){
            val filtered = product.data?.filter { prod ->
                filterTipe?.let { it == prod.tipe } ?: true
            }?.filter { prod ->
                prod.name.toLowerCase().contains(keyword.toLowerCase())
            }.orEmpty()
            Resource.Success(filtered)
        } else {
            product
        }
    }.stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5000),
        initialValue = Resource.Loading()
    )

    //Fetch products from repository
    fun getProducts(){
        viewModelScope.launch {
            _products.emit(repository.fetchProducts())
        }
    }

    fun applyFilter(tipe: String?){
        viewModelScope.launch {
            _filterTipeHazard.emit(tipe)
        }
    }

    init {
        getProducts()
    }
}