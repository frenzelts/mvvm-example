package com.emos.product.viewmodel

import androidx.lifecycle.ViewModel

import android.app.Application

import androidx.lifecycle.ViewModelProvider
import com.emos.product.repository.ProductRepository

class ProductViewModelFactory(private val repository: ProductRepository)
    : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ProductViewModel(repository) as T
    }
}