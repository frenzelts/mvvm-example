package com.emos.product.model

data class ProductModel (
    val id: Long,
    val kode: String,
    val name: String,
    val imageUrl: String,
    val price: Int,
    val uom: String,
    val kodesub: String,
    val kategori: String,
    val tipe: String
)