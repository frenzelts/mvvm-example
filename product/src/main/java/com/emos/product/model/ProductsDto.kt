package com.emos.product.model

import com.google.gson.annotations.SerializedName

data class ProductsDto(
    @SerializedName("data")
    var data: Data
){
    data class Data(
        @SerializedName("products")
        var products: List<Product>
    ){
        data class Product(
            @SerializedName("urutan")
            val id: Long,
            @SerializedName("kodeprod")
            val kode: String,
            @SerializedName("prodname")
            val name: String,
            @SerializedName("pricelist")
            val price: Int,
            @SerializedName("uom_desc")
            val uom: String,
            @SerializedName("kodesubdist")
            val kodesub: String,
            @SerializedName("sub_kategori")
            val kategori: String,
            @SerializedName("tipe")
            val tipe: String
        )
    }
}