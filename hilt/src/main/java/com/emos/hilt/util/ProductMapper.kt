package com.emos.hilt.util

import com.emos.hilt.model.ProductModel
import com.emos.hilt.model.ProductsDto

object ProductMapper: DomainModelMapper<ProductsDto.Data.Product, ProductModel> {
    override fun mapToDomain(dto: ProductsDto.Data.Product): ProductModel {
        return ProductModel(
            id = dto.id,
            name = dto.name,
            imageUrl = "https://www.emos.id/apicontent/product/111111/${dto.kode}.jpg",
            price = dto.price,
            kode = dto.kode,
            uom = dto.uom,
            kodesub = dto.kodesub,
            kategori = dto.kategori,
            tipe = dto.tipe
        )
    }

    override fun mapToDomainList(list: List<ProductsDto.Data.Product>?): List<ProductModel> {
        return list?.map { mapToDomain(it) }.orEmpty()
    }
}