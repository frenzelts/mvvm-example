package com.emos.hilt.view

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.*
import com.emos.hilt.databinding.ActivityProductBinding
import com.google.android.material.snackbar.Snackbar
import com.emos.hilt.repository.ProductRepository
import com.emos.hilt.util.ErrorSnackbar
import com.emos.hilt.util.Resource
import com.emos.hilt.viewmodel.ProductViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class HiltProductActivity : AppCompatActivity() {
    val TAG = "ProductActivity"

    private val binding by lazy { ActivityProductBinding.inflate(layoutInflater) }


    //Cara #1 (tradisional), instantiate ViewModel pakai ViewModelProvider
//    and ProductViewModelFactory to pass repository arguments (manual dependency injection)
//    private val viewmodel : ProductViewModel by lazy {
//        ViewModelProvider(this,
//            ProductViewModelFactory(ProductRepository(application))
//        ).get(ProductViewModel::class.java)
//    }

    //Cara #2, pakai kotlin delegate
    private val viewmodel : ProductViewModel by viewModels()

    private val productAdapter by lazy { ProductAdapter() }
    private val filterDialog by lazy { FilterBottomSheetDialog() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        supportActionBar?.title = "Filter Data Example"

        initListeners()
        setupRecyclerView()
        observeProducts()
    }

    private fun initListeners(){
        binding.etSearch.addTextChangedListener {
            Log.d(TAG, "initListeners: ${it.toString()}")
            viewmodel.searchKeyword(it.toString())
        }

        binding.btnFilter.setOnClickListener {
            filterDialog.show(supportFragmentManager, "filter")
        }
    }

    private fun setupRecyclerView(){
        productAdapter.apply {
            registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
                override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                    super.onItemRangeInserted(positionStart, itemCount)
                    binding.productList.layoutManager?.scrollToPosition(0)
                }

                override fun onItemRangeMoved(fromPosition: Int, toPosition: Int, itemCount: Int) {
                    super.onItemRangeMoved(fromPosition, toPosition, itemCount)
                    binding.productList.layoutManager?.scrollToPosition(0)
                }
            })
        }
        binding.productList.apply {
            layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
            itemAnimator = DefaultItemAnimator()
            setHasFixedSize(true)
            adapter = productAdapter
        }
    }

    private fun observeProducts(){
        lifecycleScope.launch {
            viewmodel.products
                .flowWithLifecycle(lifecycle)
                .collect {
                    when(it){
                        is Resource.Loading -> {
                            binding.loading.root.visibility = View.VISIBLE
                        }
                        is Resource.Error -> {
                            binding.loading.root.visibility = View.GONE
                            binding.btnFilter.visibility = View.GONE
                            ErrorSnackbar.make(
                                binding.root,
                                layoutInflater,
                                "Gagal memuat data",
                                Snackbar.LENGTH_LONG
                            ){
                                viewmodel.getProducts()
                            }.show()
                        }
                        is Resource.Success -> {
                            it.data?.let {
                                binding.loading.root.visibility = View.GONE
                                productAdapter.submitList(it)
                                binding.btnFilter.visibility = View.VISIBLE
                                if(it.isEmpty()){
                                    binding.productList.visibility = View.INVISIBLE
                                    binding.productNotFound.root.visibility = View.VISIBLE
                                    binding.tvCount.visibility = View.INVISIBLE
                                } else {
                                    binding.productList.visibility = View.VISIBLE
                                    binding.productNotFound.root.visibility = View.GONE
                                    binding.tvCount.apply {
                                        text = "Ditemukan ${it.size} produk"
                                        visibility = View.VISIBLE
                                    }
                                }
                            }
                        }
                    }
                }
        }
    }
}