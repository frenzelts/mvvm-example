package com.emos.hilt.view

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.transition.Slide
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.*
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.emos.hilt.R
import com.emos.hilt.databinding.FilterDialogBinding
import com.emos.hilt.repository.ProductRepository
import com.emos.hilt.util.Formatting
import com.emos.hilt.viewmodel.ProductViewModel
import com.google.android.material.slider.Slider
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class FilterBottomSheetDialog : BottomSheetDialogFragment() {
    val TAG = "FilterBottomSheetDialog"

    private var _binding : FilterDialogBinding? = null
    private val binding
        get() = _binding!!

//    private val viewmodel : ProductViewModel by lazy {
//        ViewModelProvider(requireActivity(),
//            ProductViewModelFactory(requireActivity().application,
//                ProductRepository(requireActivity().application))
//        ).get(ProductViewModel::class.java)
//    }

    private val viewmodel : ProductViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FilterDialogBinding.inflate(inflater, container, false)
        return _binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initListeners()
        observeTipe()
    }

    private fun initListeners(){
        binding.btnApplyFilter.setOnClickListener {
            val chipId = binding.chipGroupHazard.checkedChipId
            val tipe = if(chipId == -1) {
                null
            } else {
                binding.root.rootView.findViewById<Chip>(chipId).text.toString()
            }

            //Send the filters to viewmodel
            viewmodel.applyFilter(tipe)
            dismiss()
        }
    }

    private fun observeTipe(){
        lifecycleScope.launch{
            viewmodel.filterTipeHazard
                .flowWithLifecycle(lifecycle)
                .collect {
                    it.forEach { addChip(it.first, it.second) }
                }
        }
    }

    private fun addChip(tipe: String, checked: Boolean){
        val chip = layoutInflater.inflate(R.layout.tipe_chip_view, binding.chipGroupHazard, false) as Chip
        chip.apply {
            id = View.generateViewId()
            text = tipe
            isChecked = checked
        }
        //Add the new chip to existing chip group
        binding.chipGroupHazard.addView(chip)
    }
}