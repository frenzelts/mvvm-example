package com.emos.hilt.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.emos.hilt.databinding.ProductItemBinding
import com.emos.hilt.model.ProductModel
import com.emos.hilt.util.Formatting
import com.emos.hilt.util.ImageLoader

class ProductAdapter: ListAdapter<ProductModel, ProductAdapter.ViewHolder>(DiffCallback()) {
    val TAG = "ProductAdapter"

    companion object{
        class DiffCallback : DiffUtil.ItemCallback<ProductModel>() {
            override fun areItemsTheSame(oldItem: ProductModel, newItem: ProductModel): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: ProductModel, newItem: ProductModel): Boolean {
                return oldItem == newItem
            }
        }
    }

    inner class ViewHolder(val binding: ProductItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(model: ProductModel){
            ImageLoader.loadImage(binding.imageProduct, model.imageUrl)
            binding.tvProductName.text = model.name
            binding.tvPrice.text = "${Formatting.currencyFormat(model.price)}/${model.uom}"
            binding.tvHazard.text = model.tipe
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ProductItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}