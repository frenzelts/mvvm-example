package com.emos.hilt.repository

import android.app.Application
import android.content.Context
import com.emos.hilt.R
import com.google.gson.Gson
import com.emos.hilt.model.ProductModel
import com.emos.hilt.model.ProductsDto
import com.emos.hilt.util.ProductMapper
import com.emos.hilt.util.Resource
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.withContext
import java.io.InputStreamReader
import javax.inject.Inject

class ProductRepository @Inject constructor(
    @ApplicationContext val context: Context
) {
    val TAG = "ProductRepository"

    suspend fun fetchProducts() = withContext(Dispatchers.IO){
        Resource.Success(readJson())
    }

    private fun readJson() : List<ProductModel>{
        val stream = context.resources.openRawResource(R.raw.products)
        val reader = InputStreamReader(stream)
        val model = Gson().fromJson(reader, ProductsDto::class.java)
        reader.close()
        stream.close()
        return ProductMapper.mapToDomainList(model.data.products)
    }
}