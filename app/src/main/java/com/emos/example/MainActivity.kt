package com.emos.example

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.emos.example.databinding.ActivityMainBinding
import com.emos.hilt.view.HiltProductActivity
import com.emos.product.view.ProductActivity
import com.emos.register.RegisterActivity

class MainActivity : AppCompatActivity() {

    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        binding.btnRegister goTo RegisterActivity::class.java

        binding.btnProduct goTo ProductActivity::class.java

        binding.btnHiltProduct goTo HiltProductActivity::class.java
    }

    private fun startActivity(cls: Class<*>){
        startActivity(Intent(this, cls))
    }

    private infix fun View.goTo(cls: Class<*>) {
        setOnClickListener { startActivity(cls) }
    }
}